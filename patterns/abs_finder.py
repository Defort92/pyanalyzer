import ast


class AbstractClassAnalyzer(ast.NodeVisitor):
    def __init__(self):
        self.abstract_classes = {}
        self.all_classes = {}

    def visit_ClassDef(self, node):
        current_class = {
            'name': node.name,
            'methods': {},
            'abstract_methods': [],
            'bases': [b.id for b in node.bases if isinstance(b, ast.Name)],
            'children': []
        }

        # Проверяем, содержит ли класс абстрактные методы
        for n in node.body:
            if isinstance(n, ast.FunctionDef):
                method_details = {
                    'name': n.name,
                    'params': {},
                    'return_type': getattr(n.returns, 'id', None) if n.returns else None
                }
                # Сбор информации о параметрах и аннотациях
                for arg in n.args.args:
                    param_type = getattr(arg.annotation, 'id', None) if arg.annotation else None
                    method_details['params'][arg.arg] = param_type

                current_class['methods'][n.name] = method_details
                if any(isinstance(decorator, ast.Name) and decorator.id == 'abstractmethod' for decorator in
                       n.decorator_list):
                    current_class['abstract_methods'].append(n.name)

        if current_class['abstract_methods']:
            self.abstract_classes[node.name] = current_class
        self.all_classes[node.name] = current_class

        self.generic_visit(node)

    def post_process(self):
        # Связываем детей с их родителями
        for cls in self.all_classes.values():
            for base in cls['bases']:
                if base in self.abstract_classes:
                    self.abstract_classes[base]['children'].append(cls['name'])

    def print_abstract_classes_info(self):
        for name, cls in self.abstract_classes.items():
            print(f"Абстрактный класс: {name}")
            print(f"  Методы: {', '.join(cls['methods'].keys())}")
            print(f"  Абстрактные методы: {', '.join(cls['abstract_methods'])}")
            print(f"  Дети: {', '.join(cls['children'])}")
            for child_name in cls['children']:
                child = self.all_classes[child_name]
                print(f"    Ребенок: {child_name}")
                for method_name, method_details in child['methods'].items():
                    params = ", ".join([f"{p} ({t})" for p, t in method_details['params'].items()])
                    return_type = method_details['return_type']
                    print(f"    Метод: {method_name}({params}) -> {return_type}")


if __name__ == '__main__':
    # Тестовый код
    code = """
    from abc import ABC, abstractmethod

    class Base(ABC):
        @abstractmethod
        def do_something1(self, value: int) -> None:
            pass

        def do_something(self, value: int) -> None:
            pass

    class Derived(Base):
        def do_something(self, value: int) -> None:
            print("Doing something")
    """

    tree = ast.parse(code)
    analyzer = AbstractClassAnalyzer()
    analyzer.visit(tree)
    analyzer.post_process()
    analyzer.print_abstract_classes_info()
