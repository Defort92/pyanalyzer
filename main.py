import argparse
from radon.raw import analyze
from radon.complexity import cc_visit_ast
import ast
import os
import re

from structure import *


def main_analysis(directory_path):
    exclusions = ["node_modules", "venv", "__pycache__", ".venv", ".git", ".idea"]
    root_directory = Directory(directory_path, exclusions=exclusions)
    root_directory.analyze()
    root_directory.analyze_temporal_complexity()

    # Вывод результатов
    summary = root_directory.get_summary()
    # print(root_directory)
    # print(summary)

    print(f"путь                                 - {os.path.abspath(summary['path'])}")
    print(f"всего файлов                         - {summary['total_files']}")
    print(f"процент кода, соотвествующего pep8   - {100 - summary['files_info']['readability_score']}%")
    print(f"Процект комментариев в коде          - {summary['files_info']['comment_ratio'] * 100}%")
    print(f"Цикломатическая сложность            - {summary['files_info']['cyclomatic_complexity']}")
    print(f"Временная сложность                  - {summary['files_info']['temporal_complexity']}")
    print(f"Небезопасные импорты                 - {summary['files_info']['library_versions']}")
    if summary["files_info"]["vulnerabilities"]["Confidence"] == dict():
        print("Ошибок не было выявлено")
    else:
        print("Ошибки по важности:")
    for key, value in summary["files_info"]["vulnerabilities"]["Confidence"].items():
        print("  ", f"{key} - {value}")

    pattern_results = root_directory.detect_design_patterns()
    pattern_str = "Обнаруженные паттерны проектирования -"
    patterns = ""
    total_patterns = 0
    for pattern, instances in pattern_results.items():
        if len(instances) > 0:
            total_patterns += len(instances)
            patterns += f"/n  {pattern} - {len(instances)}"
    pattern_str += f" {total_patterns}"
    pattern_str += patterns
    print(pattern_str)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Pyanalyzer by Defort92 (https://gitlab.com/Defort92)")
    parser.add_argument("--path", type=str, required=False, help="Путь до директории для анализа")
    args = parser.parse_args()

    if not args.path:
        print("Ошибка: Параметр --path необходим.")
    else:
        print(f"Анализируется директория: {args.path}")
        main_analysis(args.path)
