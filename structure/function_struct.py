import ast
from radon.raw import analyze
from radon.complexity import cc_visit_ast

from structure.conf_struct import *
from structure.time_complexity_analyzer import analyze_complexity


class Function:
    def __init__(self, name: str = "", body: str = "", root=None, file=None):
        self.name = name
        self.body = body
        self.temporal_complexity = None
        self.cyclomatic_complexity = None
        self.root = root
        self.file = file

    def calculate(self):
        self.calculate_cyclomatic_complexity()
        self.calculate_temporal_complexity()

    def calculate_cyclomatic_complexity(self):
        # Расчет алгоритмической сложности функции, если не было рассчитано ранее
        if self.cyclomatic_complexity is None:
            self.cyclomatic_complexity = cc_visit_ast(ast.parse(self.body)).complexity
        return self.cyclomatic_complexity

    def calculate_temporal_complexity(self):
        if self.temporal_complexity is None:
            self.temporal_complexity = analyze_complexity(self, self.root)
        return self.temporal_complexity

    def __str__(self):
        return self.get_string_info()

    def get_string_info(self, level: int = 0) -> str:
        indent = BASE_INDENT * level
        scc = self.cyclomatic_complexity
        stc = self.temporal_complexity
        complexity_info = f" (Сложность: {scc})" if scc is not None else ""
        temporal_complexity_info = f" (Временная сложность: {stc})" if stc is not None else ""
        return f"{indent}Функция: {self.name}{complexity_info}{temporal_complexity_info}"
