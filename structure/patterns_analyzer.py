import ast

class DesignPatternDetector:
    def __init__(self, directory):
        self.directory = directory

    def detect(self):
        raise NotImplementedError("Each pattern detector must implement the 'detect' method.")

    def find_abstract_classes(self):
        # Возвращает список всех абстрактных классов в директории
        abstract_classes = []
        for file in self.directory.files:
            for cls in file.classes:
                if cls.is_abstract():
                    abstract_classes.append(cls)
        return abstract_classes

    def find_descendants(self, parent_class):
        # Возвращает список всех наследников заданного класса в директории
        descendants = []
        for file in self.directory.files:
            for cls in file.classes:
                if cls.is_descendant_of(parent_class):
                    descendants.append(cls)
        return descendants


class SingletonDetector(DesignPatternDetector):
    def detect(self):
        # Проверка всех классов на наличие Singleton паттерна
        singletons = []
        for file in self.directory.files:
            for cls in file.classes:
                if cls.has_private_instance_field() and cls.overrides_new():
                    singletons.append(cls)
        return singletons


class FactoryMethodDetector(DesignPatternDetector):
    def detect(self):
        # Проверка наличия паттерна Factory Method
        factories = []
        abstract_classes = self.find_abstract_classes()
        for abstract_class in abstract_classes:
            for method in abstract_class.methods:
                if method.returns_product_instance():
                    descendants = self.find_descendants(abstract_class)
                    if any(descendant.uses_class_as_product(method) for descendant in descendants):
                        factories.append(abstract_class)
        return factories


class BuilderDetector(DesignPatternDetector):
    def detect(self):
        # Проверка наличия паттерна Builder
        builders = []
        for file in self.directory.files:
            for cls in file.classes:
                if cls.is_builder() and cls.has_multiple_setters():
                    builders.append(cls)
        return builders


class AdapterDetector(DesignPatternDetector):
    def detect(self):
        # Проверка наличия паттерна Adapter
        adapters = []
        for file in self.directory.files:
            for cls in file.classes:
                if cls.uses_class_as_adaptee():
                    adapters.append(cls)
        return adapters


class ProxyDetector(DesignPatternDetector):
    def detect(self):
        # Проверка наличия паттерна Proxy
        proxies = []
        abstract_classes = self.find_abstract_classes()
        for abstract_class in abstract_classes:
            descendants = self.find_descendants(abstract_class)
            for descendant in descendants:
                if descendant.uses_other_descendant():
                    proxies.append(descendant)
        return proxies


class BridgeDetector(DesignPatternDetector):
    def detect(self):
        # Проверка наличия паттерна Bridge
        bridges = []
        abstract_classes = self.find_abstract_classes()
        for abstract_class in abstract_classes:
            descendants = self.find_descendants(abstract_class)
            for descendant in descendants:
                if descendant.is_used_in_non_child_classes(abstract_class):
                    bridges.append(descendant)
        return bridges


class StrategyDetector(DesignPatternDetector):
    def detect(self):
        # Проверка наличия паттерна Strategy
        strategies = []
        abstract_classes = self.find_abstract_classes()
        for abstract_class in abstract_classes:
            descendants = self.find_descendants(abstract_class)
            for descendant in descendants:
                if descendant.is_used_as_strategy(abstract_class):
                    strategies.append(descendant)
        return strategies


class TemplateMethodDetector(DesignPatternDetector):
    def detect(self):
        # Проверка наличия паттерна Template Method
        templates = []
        abstract_classes = self.find_abstract_classes()
        for abstract_class in abstract_classes:
            if abstract_class.has_central_coordinating_method():
                descendants = self.find_descendants(abstract_class)
                for descendant in descendants:
                    if descendant.overrides_template_methods(abstract_class):
                        templates.append(abstract_class)
        return templates


class ObserverDetector(DesignPatternDetector):
    def detect(self):
        # Проверка наличия паттерна Observer
        observers = []
        abstract_classes = self.find_abstract_classes()
        for abstract_class in abstract_classes:
            if abstract_class.is_observer():
                descendants = self.find_descendants(abstract_class)
                for descendant in descendants:
                    if descendant.is_used_as_observer(abstract_class):
                        observers.append(descendant)
        return observers


def detect_design_patterns(directory):
    detectors = [
        SingletonDetector(directory), FactoryMethodDetector(directory), BuilderDetector(directory),
        AdapterDetector(directory), ProxyDetector(directory), BridgeDetector(directory), StrategyDetector(directory),
        TemplateMethodDetector(directory), ObserverDetector(directory)
    ]
    results = {detector.__class__.__name__[:-7]: detector.detect() for detector in detectors}
    return results
