import ast
import os
import re


class ComplexityAnalyzer(ast.NodeVisitor):
    def __init__(self, function, root):
        self.function = function
        self.root = root
        self.complexity = "O(1)"
        self.loop_depth = 0
        self.return_found = False
        self.functions_complexity = {
            "sorted": "O(n log n)",
            "sort": "O(n log n)",
            "append": "O(1)",
            "insert": "O(n)",
            "pop": "O(1)",
            "remove": "O(n)",
            "index": "O(n)",
            "find": "O(n)",
            "count": "O(n)",
            "extend": "O(n)",
            "map": "O(n)",
            "filter": "O(n)",
            "reduce": "O(n)",
            "sum": "O(n)",
            "max": "O(n)",
            "min": "O(n)",
            "all": "O(n)",
            "any": "O(n)",
            "zip": "O(n)",
        }

    def visit_FunctionDef(self, node):
        self.generic_visit(node)
        return self.complexity

    def visit_Return(self, node):
        self.return_found = True

    def visit_For(self, node):
        if self.return_found:
            return
        self.loop_depth += 1
        self.generic_visit(node)
        self.loop_depth -= 1

    def visit_While(self, node):
        if self.return_found:
            return
        self.loop_depth += 1
        self.generic_visit(node)
        self.loop_depth -= 1

    def visit_Call(self, node):
        if self.return_found:
            return
        if isinstance(node.func, ast.Name):
            func_name = node.func.id
            if func_name in self.functions_complexity:
                func_complexity = self.functions_complexity[func_name]
                self._update_complexity(func_complexity)
            else:
                # Find the function in other files if not a known function
                called_function = self.find_function_in_imports(func_name)
                if called_function:
                    called_function_complexity = called_function.calculate_temporal_complexity()
                    self._update_complexity(called_function_complexity)
        self.generic_visit(node)

    def _update_complexity(self, new_complexity):
        if "log" in new_complexity:
            if self.loop_depth > 0:
                self.complexity = "O(n^{} log n)".format(self.loop_depth + 1)
            else:
                self.complexity = new_complexity
        elif "n" in new_complexity:
            if self.loop_depth > 0:
                self.complexity = "O(n^{})".format(self.loop_depth + 1)
            else:
                self.complexity = new_complexity
        else:
            if self.loop_depth > 0:
                self.complexity = "O(n^{})".format(self.loop_depth)
            else:
                self.complexity = new_complexity

    def find_function_in_imports(self, function_name):
        # Search for the function in the current file"s imports
        for imp in self.function.file.imports:
            module_path, entity_name = parse_import_path(imp)
            if entity_name == function_name:
                function = self.function.root.find_function_by_name(module_path, function_name)
                if function:
                    return function
        # If not found in imports, return a dummy function with O(1) complexity
        return None

def analyze_complexity(function, root):
    try:
        tree = ast.parse(function.body)
    except SyntaxError as e:
        print(f"SyntaxError in the provided code: {e}")
        return None

    analyzer = ComplexityAnalyzer(function, root)
    for node in tree.body:
        if isinstance(node, ast.FunctionDef):
            complexity = analyzer.visit_FunctionDef(node)
            if analyzer.return_found:
                return "O(1)"
            return complexity

    return None


def parse_import_path(import_path):
    if import_path.startswith("import "):
        path = import_path[len("import "):].strip()
        parts = path.split(".")
        module_path = ".".join(parts[:-1])
        entity_name = parts[-1]
    elif import_path.startswith("from "):
        match = re.search(r"from (.+) import (.+)", import_path)
        if match:
            module_path = match.group(1).strip()
            entity_name = match.group(2).strip()
        else:
            raise ValueError("Invalid import path format")
    else:
        raise ValueError("Invalid import path format")
    return module_path, entity_name
