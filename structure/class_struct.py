from structure.conf_struct import *
# from structure.function_struct import Function


class Class:
    def __init__(self, name: str = ""):
        self.name = name
        self.methods = {}  # Список методов
        self.inner_classes = {}  # Список вложенных классов
        self.temporal_complexity = {}

    def find_function(self, function_name):
        # Добавление метода в класс
        function = self.methods.get(function_name)
        if function is not None:
            return function

        for inner_class_name, inner_class in self.inner_classes.items():
            function = inner_class.find_function(function_name)
            if function is not None:
                return function

        return None

    def add_method(self, function):
        # Добавление метода в класс
        self.methods[function.name] = function

    def add_inner_class(self, cls):
        # Добавление внутреннего класса
        self.inner_classes[cls.name] = cls

    def calculate_complexity(self):
        # Рекурсивный расчет сложности для всех методов и вложенных классов
        for method_name, method in self.methods.items():
            method.calculate()

        for class_name, cls in self.inner_classes.items():
            cls.calculate_complexity()

    def analyze_temporal_complexity(self):
        # Рекурсивный вызов анализа временной сложности для всех методов и вложенных классов
        for method in self.methods.values():
            method.calculate_temporal_complexity()

        for inner_cls in self.inner_classes.values():
            inner_cls.analyze_temporal_complexity()

    def update_temporal_complexity(self):
        for method in self.methods.values():
            method_temp_comp = method.temporal_complexity
            if self.temporal_complexity.get(method_temp_comp) is None:
                self.temporal_complexity[method_temp_comp] = 1
                return

            self.temporal_complexity[method_temp_comp] += 1

        for inner_class in self.inner_classes.values():
            inner_class.update_temporal_complexity()
            self.params["temporal_complexity"] = merge_dictionaries(
                self.params["temporal_complexity"], inner_class.temporal_complexity
            )

    def __str__(self):
        return self.get_string_info()

    def get_string_info(self, level: int = 0) -> str:
        indent = BASE_INDENT * level
        class_info = f"Класс: {self.name}\n"
        for method_name, method in self.methods.items():
            class_info += method.get_string_info(level + 1) + "\n"
        for inner_cls_name, inner_cls in self.inner_classes.items():
            class_info += inner_cls.get_string_info(level + 1) + "\n"
        return class_info.strip()
