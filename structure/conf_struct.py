BASE_INDENT = "  |"
BASE_DIR_INDENT = "~~|"
BASE_FILE_INDENT = "--|"

BAD_IMPORTS_WITH_WEIGHTS = {
    "subprocess": 1.0,
    "pickle": 1.0,
    "os.system": 1.0,
    "eval": 1.0,
    "exec": 1.0,
    "compile": 1.0,
    "yaml.load": 1.0,
    "xml.etree.ElementTree": 0.5,
    "socket": 0.2,
    "sqlite3": 0.5,
    "shlex": 0.2
}


def merge_dictionaries(dict1, dict2):
    merged_dict = dict1.copy()

    for key, value in dict2.items():
        if key in merged_dict:
            merged_dict[key] += value
        else:
            merged_dict[key] = value

    return merged_dict
