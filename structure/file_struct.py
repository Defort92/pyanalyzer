import subprocess
import re
import ast
from radon.raw import analyze
from radon.complexity import cc_visit_ast
from structure.class_struct import Class
from structure.function_struct import Function
from structure.conf_struct import *

class File:
    def __init__(self, path, root):
        self.path = path
        self.root = root
        self.functions = []
        self.classes = []
        self.imports = []
        self.params = {}
        self.initialize_params()
        self.parse_contents()

    def initialize_params(self):
        self.params["readability_score"] = None
        self.params["comment_ratio"] = None
        self.params["cyclomatic_complexity"] = None
        self.params["temporal_complexity"] = {}
        self.params["vulnerabilities"] = None
        self.params["vulnerabilities_more_info"] = None
        self.params["vulnerabilities_issue"] = None
        self.params["library_versions"] = None

    def visit(self, node, parent_stack, classes, functions):
        parent_stack.append(node)
        if isinstance(node, ast.FunctionDef):
            if isinstance(parent_stack[-2], ast.ClassDef):
                last_class = next((x for x in reversed(parent_stack[:-1]) if isinstance(x, ast.ClassDef)), None)
                if last_class:
                    class_object = next((obj for obj in classes if obj.name == last_class.name), None)
                    if class_object:
                        new_method = Function(node.name, ast.unparse(node), root=self.root, file=self)
                        class_object.add_method(new_method)
            else:
                new_function = Function(node.name, ast.unparse(node), root=self.root, file=self)
                functions.append(new_function)
        elif isinstance(node, ast.ClassDef):
            new_class = Class(node.name)
            classes.append(new_class)
        elif isinstance(node, ast.Import) or isinstance(node, ast.ImportFrom):
            self.imports.append(ast.unparse(node))
        for child in ast.iter_child_nodes(node):
            self.visit(child, parent_stack, classes, functions)
        parent_stack.pop()

    def parse_contents(self):
        with open(self.path, "r", encoding="utf-8") as file:
            content = file.read()
        tree = ast.parse(content)
        parent_stack = []
        self.classes = []
        self.functions = []
        self.visit(tree, parent_stack, self.classes, self.functions)

    def find_function(self, function_name):
        for func in self.functions:
            if func.name == function_name:
                return func
        for cls in self.classes:
            func = cls.find_function(function_name)
            if func:
                return func
        for imp in self.imports:
            imported_module = self.get_imported_module(imp)
            if imported_module:
                func = self.root.find_functions(imported_module.split("."), function_name, [function_name])
                if func:
                    return func[0] if func else None
        return None

    def find_class(self, class_name):
        for cls in self.classes:
            if cls.name == class_name:
                return cls
        for imp in self.imports:
            imported_module = self.get_imported_module(imp)
            if imported_module:
                cls = self.root.find_classes(imported_module.split("."), class_name, [class_name])
                if cls:
                    return cls[0] if cls else None
        return None

    def get_imported_module(self, imp):
        match = re.search(r"from (.+) import", imp)
        if match:
            return match.group(1)
        match = re.search(r"import (.+)", imp)
        if match:
            return match.group(1)
        return None

    def analyze(self):
        # Проведение всех анализов для файла
        self.analyze_readability()
        self.analyze_comment_ratio()
        self.analyze_cyclomatic_complexity()
        self.check_vulnerabilities()
        self.check_libraries()

    def analyze_temporal_complexity(self):
        # Анализ временной сложности для всех функций и классов в файле
        for function in self.functions:
            function.calculate_temporal_complexity()
        for cls in self.classes:
            cls.analyze_temporal_complexity()

    def analyze_readability(self):
        result = subprocess.run(["black", "--check", self.path], capture_output=True, text=True)
        total_lines = 0
        non_compliant_lines = 0

        with open(self.path, "r", encoding="utf-8") as file:
            content = file.read()
            total_lines = analyze(content).lloc

        if result.returncode != 0:
            output_lines = result.stdout.split("\n")
            non_compliant_lines = sum(1 for line in output_lines if "would reformat" in line)

        readability_score = (non_compliant_lines / total_lines) * 100 if total_lines > 0 else 0

        self.params["readability_score"] = readability_score

    def analyze_comment_ratio(self):
        with open(self.path, "r", encoding="utf-8") as file:
            content = file.read()
        stats = analyze(content)  # Получаем статистику из содержимого файла
        total_lines = stats.lloc  # Количество строк кода, а вообще всех
        comment_lines = stats.comments
        comment_ratio = comment_lines / total_lines if total_lines > 0 else 0  # Избегаем деления на ноль
        self.params["comment_ratio"] = comment_ratio

    def analyze_cyclomatic_complexity(self):
        with open(self.path, "r", encoding="utf-8") as file:
            content = file.read()

        # Парсинг файла для создания AST (Abstract Syntax Tree)
        tree = ast.parse(content)

        # Использование radon для подсчета цикломатической сложности
        complexity = cc_visit_ast(tree)
        # Сохранение результатов в параметрах
        self.params["cyclomatic_complexity"] = 0
        for comp in complexity:
            self.params["cyclomatic_complexity"] += comp.complexity

    def check_vulnerabilities(self):
        result = subprocess.run(["bandit", "-q", "-r", self.path], capture_output=True, text=True)

        # Парсинг результатов bandit с использованием регулярных выражений
        issues_pattern = r">> Issue: \[(.+?)\] (.+?)\n   Severity: (.+?)   Confidence: (.+?)\n"
        matches = re.finditer(issues_pattern, result.stdout)

        vulnerabilities_list = []
        for match in matches:
            issue_info = {
                "Severity": match.group(3),
                "Confidence": match.group(4)
            }
            vulnerabilities_list.append(issue_info)

        # Сохраняем результаты в параметры объекта
        self.params["vulnerabilities"] = vulnerabilities_list
        if vulnerabilities_list:
            self.params["vulnerabilities_more_info"] = vulnerabilities_list[0].get("More Info", "No details provided")
            self.params["vulnerabilities_issue"] = vulnerabilities_list[0].get("Issue", "No issue details")

    def check_libraries(self, requirements_path: str = ""):
        import_string = "\n".join(map(str, self.imports))
        # Определяем список "плохих" импортов

        # Парсинг строки импортов в абстрактное синтаксическое дерево (AST)
        tree = ast.parse(import_string)

        # Инициализация списка для хранения найденных плохих импортов с весами
        findings = []

        # Проход по всем узлам AST и поиск импортов
        for node in ast.walk(tree):
            if isinstance(node, ast.Import):
                for alias in node.names:
                    if alias.name in BAD_IMPORTS_WITH_WEIGHTS:
                        findings.append((alias.name, BAD_IMPORTS_WITH_WEIGHTS[alias.name]))
            elif isinstance(node, ast.ImportFrom):
                if node.module in BAD_IMPORTS_WITH_WEIGHTS:
                    findings.append((node.module, BAD_IMPORTS_WITH_WEIGHTS[node.module]))
        total_weight = sum(weight for _, weight in findings)
        self.params["library_versions"] = total_weight

    def update_temporal_complexity(self):
        for function in self.functions:
            func_temp_comp = function.temporal_complexity
            if self.params["temporal_complexity"].get(func_temp_comp) is None:
                self.params["temporal_complexity"][func_temp_comp] = 1
                return

            self.params["temporal_complexity"][func_temp_comp] += 1

        for inner_class in self.classes:
            inner_class.update_temporal_complexity()
            self.params["temporal_complexity"] = merge_dictionaries(
                self.params["temporal_complexity"], inner_class.temporal_complexity
            )

    def get_summary(self):
        self.update_temporal_complexity()
        return self.params

    def __str__(self):
        return self.get_string_info()

    def get_string_info(self, level: int = 0) -> str:
        indent = BASE_INDENT * level
        file_indent = BASE_FILE_INDENT * level
        indent_one_more = BASE_INDENT * (level + 1)
        final_classes_info = ""
        final_functions_info = ""

        classes_info = f"\n{indent}".join(cls.get_string_info(level + 1) for cls in self.classes)
        functions_info = f"\n{indent}".join(func.get_string_info(level + 1) for func in self.functions)
        params_info = f"\n{indent_one_more}".join(f"{key}: {value}" for key, value in self.params.items())

        if len(self.classes) > 0:
            final_classes_info = f"{indent}Классы:\n{indent_one_more}{classes_info}\n"
        if len(self.functions) > 0:
            final_functions_info = f"{indent}Функции:\n{indent_one_more}{functions_info}\n"

        final_string = (f"{file_indent}Файл: {self.path} ({self.imports})\n"
                        f"{indent}Параметры:\n{indent_one_more}{params_info}\n"
                        f"{final_classes_info}\n"
                        f"{final_functions_info}"
                        f"{file_indent}//\n\n")
        return final_string
