import os
from structure.file_struct import File
from structure.conf_struct import *
from structure.patterns_analyzer import detect_design_patterns

class Directory:
    def __init__(self, path: str, root=None, exclusions: list | None = None):
        self.name = os.path.basename(path)
        self.path = path
        self.root = root if root else self
        self.files = []
        self.subdirectories = []
        self.exclusions = exclusions if exclusions else []
        self.load_contents()

    def load_contents(self):
        for entry in os.scandir(self.path):
            if entry.name in self.exclusions:
                continue
            if entry.is_file() and entry.name.endswith(".py"):
                self.files.append(File(entry.path, self.root))
            elif entry.is_dir():
                self.subdirectories.append(Directory(entry.path, self.root, self.exclusions))

    def find_file(self, parts):
        if not parts:
            return None
        if len(parts) == 1:
            file_name = parts[0] + ".py"
            file = next((f for f in self.files if os.path.basename(f.path) == file_name), None)
            return file
        else:
            subdirectory_name = parts[0]
            subdirectory = next((d for d in self.subdirectories if d.name == subdirectory_name), None)
            if subdirectory:
                return subdirectory.find_file(parts[1:])
            return None

    def find_entity(self, directories, file, entities, entity_type):
        parts = directories + [file]
        target_file = self.find_file(parts)
        if target_file:
            results = []
            for entity_name in entities:
                if entity_type == "function":
                    entity = target_file.find_function(entity_name)
                else:
                    entity = target_file.find_class(entity_name)
                if entity:
                    results.append(entity)
            return results
        for subdir in self.subdirectories:
            results = subdir.find_entity(directories, file, entities, entity_type)
            if results:
                return results
        return None

    def find_functions(self, directories, file, functions):
        return self.find_entity(directories, file, functions, "function")

    def find_classes(self, directories, file, classes):
        return self.find_entity(directories, file, classes, "class")

    def find_function_by_name(self, module_path, function_name):
        parts = module_path.split(".")
        target_file = self.find_file(parts)
        if target_file:
            function = target_file.find_function(function_name)
            if function:
                return function
        for subdirectory in self.subdirectories:
            function = subdirectory.find_function_by_name(module_path, function_name)
            if function:
                return function
        return None

    def analyze(self):
        for file in self.files:
            file.analyze()
        for subdirectory in self.subdirectories:
            subdirectory.analyze()

    def analyze_temporal_complexity(self):
        # Анализ временной сложности для всех файлов и поддиректорий
        for file in self.files:
            file.analyze_temporal_complexity()
        for subdirectory in self.subdirectories:
            subdirectory.analyze_temporal_complexity()

    def get_summary(self):
        files_summary = [file.get_summary() for file in self.files]
        directories_summary = [subdirectory.get_summary() for subdirectory in self.subdirectories]

        summary = {
            "path": self.path,
            "total_files": len(self.files),
            "files_info": {
                "readability_score": 0,
                "comment_ratio": 0,
                "cyclomatic_complexity": 0,
                "temporal_complexity": {},
                "library_versions": 0,
                "vulnerabilities": {
                    "Confidence": {},
                    "Severity": {},
                },
            },
        }

        total_files = len(self.files)
        for file_summary in files_summary:
            summary["files_info"]["readability_score"] += (file_summary["readability_score"] / total_files)
            summary["files_info"]["comment_ratio"] += (file_summary["comment_ratio"] / total_files)
            summary["files_info"]["cyclomatic_complexity"] += file_summary["cyclomatic_complexity"]
            summary["files_info"]["library_versions"] += file_summary["library_versions"]

            for temporal_comp, value in file_summary["temporal_complexity"].items():
                if summary["files_info"]["temporal_complexity"].get(temporal_comp) is None:
                    summary["files_info"]["temporal_complexity"][temporal_comp] = value
                    continue

                summary["files_info"]["temporal_complexity"][temporal_comp] += value

            for vulnerability in file_summary["vulnerabilities"]:
                confidence = vulnerability["Confidence"]
                severity = vulnerability["Severity"]
                vulns = summary["files_info"]["vulnerabilities"]

                if vulns["Confidence"].get(confidence, None) is not None:
                    vulns["Confidence"][confidence] += 1
                else:
                    vulns["Confidence"][confidence] = 1

                if vulns["Severity"].get(severity, None) is not None:
                    vulns["Severity"][severity] += 1
                else:
                    vulns["Severity"][severity] = 1

        total_dirs = len(self.subdirectories)
        summary["files_info"]["readability_score"] /= total_dirs if total_dirs != 0 else 1
        summary["files_info"]["comment_ratio"] /= total_dirs if total_dirs != 0 else 1
        for dir_summary in directories_summary:
            summary["total_files"] += dir_summary["total_files"]
            summary["files_info"]["readability_score"] += (dir_summary["files_info"]["readability_score"] / total_dirs)
            summary["files_info"]["comment_ratio"] += (dir_summary["files_info"]["comment_ratio"] / total_dirs)
            summary["files_info"]["cyclomatic_complexity"] += dir_summary["files_info"]["cyclomatic_complexity"]

            for temporal_comp, value in dir_summary["files_info"]["temporal_complexity"].items():
                if summary["files_info"]["temporal_complexity"].get(temporal_comp) is None:
                    summary["files_info"]["temporal_complexity"][temporal_comp] = value
                    continue

                summary["files_info"]["temporal_complexity"][temporal_comp] += value

            vulns = summary["files_info"]["vulnerabilities"]
            for key, value in dir_summary["files_info"]["vulnerabilities"]["Confidence"].items():
                vulns["Confidence"][key] = value + vulns["Confidence"].get(key, 0)
            for key, value in dir_summary["files_info"]["vulnerabilities"]["Severity"].items():
                vulns["Severity"][key] = value + vulns["Severity"].get(key, 0)

            summary["files_info"]["library_versions"] += dir_summary["files_info"]["library_versions"]

        return summary

    def detect_design_patterns(self):
        return detect_design_patterns(self)
