import os
import black

def check_black_formatting(file_path):
    with open(file_path, "r", encoding="utf-8") as f:
        code = f.read()
        try:
            formatted_code = black.format_file_contents(code, fast=False, mode=black.FileMode())
            if formatted_code == code:
                print(f"Файл {file_path} уже соответствует стандартам форматирования Black.")
            else:
                original_lines = code.split("\n")
                formatted_lines = formatted_code.split("\n")
                num_lines_changed = sum(
                    1 for orig, formatted in zip(original_lines, formatted_lines) if orig != formatted
                )
                print(f"В файле {file_path} требуется изменить {num_lines_changed} строк, чтобы соответствовать стандартам форматирования Black.")
        except black.InvalidInput:
            print(f"Неверный ввод в файле {file_path}.")
        except black.NothingChanged:
            print(f"Файл {file_path} уже соответствует стандартам форматирования Black.")

def analyze_directory(directory):
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith(".py"):
                file_path = os.path.join(root, file)
                print(f"Анализ файла: {file_path}")
                check_black_formatting(file_path)
                print("")


if __name__ == "__main__":
    directory_path = "./"  # Замените на путь к нужной директории
    analyze_directory(directory_path)
