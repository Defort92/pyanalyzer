import os
import glob
import subprocess
import radon.raw


def check_pep8_compliance(filename):
    try:
        black_result = subprocess.run(['black', '--check', filename], capture_output=True, text=True)
        is_compliant = black_result.returncode == 0
    except subprocess.CalledProcessError as e:
        print(f"Ошибка при проверке файла {filename}: {str(e)}")
        return None, None

    with open(filename, 'r') as file:
        content = file.read()
    analysis = radon.raw.analyze(content)
    total_lines = analysis.loc - analysis.blank - analysis.single_comments - analysis.multi

    if is_compliant:
        return total_lines, 0
    else:
        # Подсчет ошибочных строк - требуется детальный анализ вывода black
        error_lines = estimate_error_lines(black_result.stderr)
        return total_lines, error_lines

def estimate_error_lines(black_output):
    # Здесь должна быть реализована логика для подсчета строк, не соответствующих PEP8
    return 10  # Пока что ставим фиксированное значение для демонстрации

def analyze_directory(directory_path):
    python_files = glob.glob(os.path.join(directory_path, '**', '*.py'), recursive=True)
    total_lines = 0
    total_error_lines = 0

    for file in python_files:
        file_lines, file_error_lines = check_pep8_compliance(file)
        if file_lines is not None:
            total_lines += file_lines
            total_error_lines += file_error_lines

    if total_lines > 0:
        compliance_percentage = 100 * (1 - total_error_lines / total_lines)
    else:
        compliance_percentage = 100

    return compliance_percentage

# Путь к директории для анализа
# directory_path = '.'
# compliance = analyze_directory(directory_path)
# print(f"Общий процент соответствия PEP8: {compliance}%")
